﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RomanNumbers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ConvertIt_Click(object sender, RoutedEventArgs e)
        {

            int numberToConvert = Convert.ToInt32(IntegerNumber.Text);
            string romanNumber = "";

            string[] romanDigit = { "M", "CM", "D", "CD", "C", "XC",
                "L", "XL", "X", "IX", "V", "IV", "I" };
            int[] digitValue = {1000,900,500,400,100,90,
                50, 40, 10, 9, 5, 4, 1 };
            int numDigits = romanDigit.Length;

            int currentValue = numberToConvert;


            for (int count = 0; count < numDigits; count++)
            {
                romanNumber = romanNumber + GetRomanDigits(ref currentValue,
                   digitValue[count], romanDigit[count]);
            }

            Result.Text = romanNumber;
        } // end Convert Button Click

        string GetRomanDigits(ref int current, int value, string digit)
        {
            string result = "";
            while (current >= value)
            {
                result = result + digit;
                current = current - value;
            }
            return result;
        } // end Get Roman Digits

    }// end class
} // end namespace
